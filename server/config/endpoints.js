const initalizeEndpoints = (app) => {
    /**
     * @swagger
     *
     * /api/login:
     *   post:
     *     tags: [ "Users" ]
     *     description: Login to the application
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: username
     *         description: Username to use for login.
     *         in: formData
     *         required: true
     *         type: string
     *       - name: password
     *         description: User's password.
     *         in: formData
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         description: login
     */
    app.get('/api/login', (req, res) => res.end('this get all '));
}

module.exports = initalizeEndpoints;