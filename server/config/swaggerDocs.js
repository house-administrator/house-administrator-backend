const swaggerUi = require('swagger-ui-express');
const swaggerJsdocs = require('swagger-jsdoc');

const options = {
    swaggerDefinition: {
        info: {
            title: 'Test API',
            version: '1.0.0',
            description: 'Test Express API with autogenerate swagger doc'
        },
        basePath: '/'
    },
    apis: ['./server/config/endpoints.js'],
};

const specs = swaggerJsdocs(options);

module.exports = (app) => {
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs))
}