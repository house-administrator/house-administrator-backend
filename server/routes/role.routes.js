const express = require('express');
const router = express.Router();

const roleCtrl = require('../controllers/role.controller');

router.get('/', roleCtrl.getAll);

module.exports = router;