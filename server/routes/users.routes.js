const express = require('express');
const router = express.Router();

const userCtrl = require('../controllers/users.controller');

router.get('/', userCtrl.getUsers);
router.post('/', userCtrl.createUser);
router.put('/:id', userCtrl.updateUser);
router.get('/:id', userCtrl.getUser);
router.delete('/:id', userCtrl.deleteUser);

module.exports = router;