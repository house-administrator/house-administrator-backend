const express = require('express');
const router = express.Router();

const companyCtrl = require('../controllers/company.controller');

router.get('/', companyCtrl.getCompanies);
router.post('/', companyCtrl.createCompany);
router.put('/:id', companyCtrl.updateCompany);
router.get('/:id', companyCtrl.getCompany);
router.delete('/:id', companyCtrl.deleteCompany);

module.exports = router;