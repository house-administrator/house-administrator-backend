const company = require('../models/company');
const companiesCtrl = {};

companiesCtrl.getCompanies = async (req, res) => {
    const companies = await company.find();
    res.json(companies);
};

companiesCtrl.createCompany = async (req, res) => {
    const newCompany = company(res.body);
    await newCompany.save();
    res.json({
        status: "Company saved"
    });
};

companiesCtrl.updateCompany = async (req, res) => {
    const { id } = req.params.id;
    const editCompany = {
        name: req.body.name,
        password: req.body.password
    };
    await company.findByIdAndUpdate({_id: id}, {$set: editCompany}, {new: true, useFindAndModify: false});
    res.json({
        status: "Company updated"
    })
};

companiesCtrl.getCompany = async (req, res) => {
    const getCompany = await company.findById(req.params.id);
    res.json(getCompany)
};

companiesCtrl.deleteCompany = async (req, res) => {
    await company.findByIdAndRemove(req.params.id);
    res.json({
        stauts: 'Company deleted'
    })    
};

module.exports = companiesCtrl;