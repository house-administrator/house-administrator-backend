const user = require('../models/users');
const role = require('../_helpers/role');
const auth = require('../services/auth.service');
const bcrypt = require('bcrypt');
const userCtrl = {};

userCtrl.signup = (req, res) => {
    bcrypt.hash(req.body.password, 10, function (err, hash) {
        const new_User = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            login: req.body.login,
            email: req.body.email,
            password: hash,
            role: [role.ROLE_USER],
            imageUrl: req.body.imageUrl,
            langKey: 'en',
        };
        const newUser = user(new_User);
        newUser.save((err) => {
            if (err) {
                res.status(500).send({ message: `Error al crear el usuario ${err}` });
            }
            return res.status(200).send({ token: auth.createToken(newUser) })
        });
    });
};

userCtrl.signin = async (req, res) => {
    await user.find({ email: req.body.email }, async (err, user1) => {
        if (await err) {
            res.status(500).send({ message: err })
        }
        console.log(user1);
        if (await user1.length == 0) {
            res.status(404).send({ message: 'No existe el usuarior' })
        } else {
            const password = user1[0].password;
            bcrypt.compare(req.body.password, password, async (err, result) => {
                if (await err) {
                    res.status(400).send({ message: err });
                }
                if (await !result) {
                    res.status(400).send({ message: 'El password es incorrecto' });
                } else {
                    res.user1 = user1;
                    res.status(200).send({
                        message: 'Te has autenticado exitosamente',
                        token: auth.createToken(user1)
                    })
                }
            });
        }
    });
}

module.exports = userCtrl;