const user = require('../models/users');
const usersCtrl = {};

usersCtrl.getUsers = async (req, res) => {
    const users = await user.find();
    res.json(users)
};

usersCtrl.createUser = async (req, res) => {
    const newUser = user(req.body); 
    console.log("La respuesta es recivida --> ", newUser);
    await newUser.save();
    res.json({
        status: "User saved"
    });
};

usersCtrl.updateUser = async (req, res) => {
    const { id } = req.params;
    const editUser = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        login: req.body.login,
        email: req.body.email,
        password: req.body.password,
        role: req.body.role
    }
    await user.findByIdAndUpdate({_id: id}, {$set: editUser}, {new: true, useFindAndModify: false});
    res.json({
        status: 'User updated'
    });
};

usersCtrl.getUser = async (req, res) => {
    const getUser = await user.findById(req.params.id);
    res.json(getUser);
};

usersCtrl.deleteUser = async (req, res) => {
    await user.findByIdAndRemove(req.params.id);
    res.json({
        status: 'User deleted'
    });
};

module.exports = usersCtrl;