const role = require('../_helpers/role');

const roleCtrl = {};

roleCtrl.getAll = async (req, res) => {
    res.json(role);
}

module.exports = roleCtrl;