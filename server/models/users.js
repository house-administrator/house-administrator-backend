const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
    firstName: { 
        type: String
    },
    lastName: {
        type: String
    },
    login: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
    },
    role: {
        type: []
    },
    imageUrl: {
        type: String
    },
    langKey: {
        type: String
    },
    activationKey: {
        type: String
    },
    resetKey: {
        type: String
    },
    createBy: {
        type: String
    },
    createDate: {
        type: Date
    },
    lastModifieldBy: {
        type: String
    },
    lastModifieldDate: {
        type: Date
    }
});

module.exports = mongoose.model('users', userSchema);